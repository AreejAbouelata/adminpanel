<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_text') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_text',old('ar_text'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان باللغه العربيه</label>
    </div>
    

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_text') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_text',old('en_text'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان باللغه الانجليزيه</label>
    </div>


    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_body') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_body',old('ar_body'),['class'=>'form-control'])!!}
        <label for="form_control_1">الموضوع باللغه العربيه</label>
    </div>


    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_body') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_body',old('en_body'),['class'=>'form-control'])!!}
        <label for="form_control_1">الموضوع باللغه الانجليزيه</label>
    </div>


    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('img') ? ' has-error' : 'has-success' }} ">
        @if(isset($Slider))
        <img src="{!! url($Slider->img) !!}" width="100px">
        @endif
        {!!Form::file('img',null,['class'=>'form-control'])!!}
        <label for="form_control_1">الصوره</label>
    </div>

    <div class="form-actions noborder">
    <button type="submit" class="btn blue">حفظ</button>
    <button type="reset" class="btn default">الغاء</button>
</div>
</div>
