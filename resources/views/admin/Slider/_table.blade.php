<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center"> الصوره</th>
        <th class="all  text-center">العنوان بالعربيه</th>
        <th class="all  text-center">العنوان بالانجليزيه</th>
        <th class="min-phone-l  text-center">الموضوع بالعربى</th>
        <th class="min-phone-l  text-center">الموضوع بالانجليزيه</th>
        <th class="min-phone-l  text-center">الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($Sliders as $Slider)
        <tr>
           <td> <img src="{!! url($Slider->img) !!}" alt="{!!$Slider->ar_title!!}" width="100px"> </td>
            <td>{!!$Slider->ar_text!!}</td>
            <td>{!!$Slider->en_text!!}</td>
            <td>{!!mb_substr(strip_tags($Slider->ar_body),0,191)!!}</td>
            <td>{!!mb_substr(strip_tags($Slider->en_body),0,191)!!}</td>
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('Slider.edit',[$Slider->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil"></i> edit</a>
                </div>
                <div class="margin-bottom-5 col-xs-6">
                    <form method="POST" action=
                            {!!route('Slider.destroy',[$Slider->id])!!}>
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"  class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-danger"></i> delete</button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>