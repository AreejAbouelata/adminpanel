<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center"> العنوان بالعربى</th>
        <th class="min-phone-l  text-center">الموضوع بالعربى</th>
        <th class="all  text-center">العنوان بالانجليزيه</th>
        <th class="min-phone-l  text-center">الموضوع بالانجليزيه</th>
        <th class="min-phone-l  text-center">الاعدادت</th>


    </tr>
    </thead>
    <tbody>
    @foreach($abouts as $about)
        <tr>
            <td>{!!$about->ar_name!!}</td>
            <td>{!!mb_substr(strip_tags($about->ar_body),0,191)!!}</td>
            <td>{!!$about->en_name!!}</td>
            <td>{!!mb_substr(strip_tags($about->en_body),0,191)!!}</td>
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('About.edit',[$about->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil"></i> edit</a>
                </div>

                <div class="margin-bottom-5">
                    <form method="POST" action=
                            {!!route('About.destroy',[$about->id])!!}>
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"  class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-danger"></i> delete</button>

                    </form>
                </div>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>