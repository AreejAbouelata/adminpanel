<div class="form-body">


    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('email') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('email',old('email'),['class'=>'form-control','disabled'])!!}
        <label for="form_control_1">الموضوع البريد الالكترونى</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('name',old('name'),['class'=>'form-control','disabled'])!!}
        <label for="form_control_1">الموضوع البريد الالكترونى</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('subject') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('subject',old('subject'),['class'=>'form-control','disabled'])!!}
        <label for="form_control_1">الموضوع</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label">
        {!!Form::textarea('message',Null,['class'=>'form-control','disabled'])!!}
        <label for="form_control_1">الرساله</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label {{ $errors->has('reply') ? ' has-error' : 'has-success' }} ">
        {!!Form::textarea('reply',old('reply'),['class'=>'form-control summernote'])!!}
        <label for="form_control_1">الرد</label>
    </div>

    <div class="form-actions noborder">
        <button type="submit" class="btn blue">حفظ</button>
        <button type="reset" class="btn default">الغاء</button>
    </div>
</div>
