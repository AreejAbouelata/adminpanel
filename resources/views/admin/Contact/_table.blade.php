<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center"> الاسم</th>
        <th class="all  text-center">الموضوع</th>
        <th class="all  text-center">البريد الالكترونى</th>
        <th class="min-phone-l  text-center">الرساله</th>
        <th class="min-phone-l  text-center">الرد</th>
        <th class="min-phone-l  text-center">الاعدادت</th>
    </tr>
    </thead>
    <tbody>
    @foreach($Contacts as $Contact)
        <tr>
           <td> {!! $Contact->name !!} </td>
            <td>{!!$Contact->subject!!}</td>
            <td> <a href="mailto:{!! $Contact->email !!}?Subject={!! $Contact->subject !!}" target="_top">{!!$Contact->email!!}</a></td>
            <td>{!!$Contact->message!!}</td>
            <td>{!! $Contact->reply !!}</td>
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('Contact.edit',[$Contact->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil"></i> edit</a>
                </div>
                <div class="margin-bottom-5">
                    <form method="POST" action=
                            {!!route('Contact.destroy',[$Contact->id])!!}>
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"  class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-danger"></i> delete</button>
                    </form>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>