<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center"> العنوان بالعربى</th>
        <th class="min-phone-l  text-center">الموضوع بالعربى</th>
        <th class="all  text-center">النسبه</th>
      
        <th class="min-phone-l  text-center">الاعدادت</th>


    </tr>
    </thead>
    <tbody>
    @foreach($Statistics as $Statistic)
        <tr>
            <td>{!!$Statistic->ar_name!!}</td>
            <td>{!!$Statistic->en_name!!}</td>
            <td>{!! $Statistic->percent!!}</td>
            <td colspan="1">
                <span class="margin-bottom-5 col-xs-6">
                    <a href="{{route('Statistic.edit',[$Statistic->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil"></i> edit</a>
                </span>

                <span class="margin-bottom-5">
                    <form method="POST" action=
                            {!!route('Statistic.destroy',[$Statistic->id])!!}>
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button type="submit"  class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-danger"></i> delete</button>
                    </form>
                </span>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>