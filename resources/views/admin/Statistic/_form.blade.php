<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_name',old('ar_name'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان باللغه العربيه</label>
    </div>
    

<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_name',old('en_name'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان  باللغه الانجليزيه</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label{{ $errors->has('en_body') ? ' has-error' : 'has-success' }}">
        {!!Form::number('percent',old('percent'),['class'=>'form-control','max'=>'100','min'=>1])!!}
        <label for="form_control_1">الموضوع  باللغه الانجليزيه</label>
    </div>
</div>

<div class="form-actions noborder">
    <button type="submit" class="btn blue">حفظ</button>
    <button type="reset" class="btn default">الغاء</button>
</div>
