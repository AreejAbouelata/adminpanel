<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_name',old('ar_name'),['class'=>'form-control'])!!}
        <label for="form_control_1">الاسم باللغه العربيه</label>
    </div>
    

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_name') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_name',old('en_name'),['class'=>'form-control'])!!}
        <label for="form_control_1">الاسم  باللغه الانجليزيه</label>
    </div>
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_job') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_job',old('ar_job'),['class'=>'form-control'])!!}
        <label for="form_control_1">الوظيفه باللغه العربيه</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_job') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_job',old('en_job'),['class'=>'form-control'])!!}
        <label for="form_control_1">الوظيفه  باللغه الانجليزيه</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('rating') ? ' has-error' : 'has-success' }} ">
        {!!Form::number('rating',old('rating'),['class'=>'form-control'])!!}
        <label for="form_control_1">التقيم</label>
    </div>
    
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('ar_title') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('ar_title',old('ar_title'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان باللغه العربيه</label>
    </div>
    
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('en_title') ? ' has-error' : 'has-success' }} ">
        {!!Form::text('en_title',old('en_title'),['class'=>'form-control'])!!}
        <label for="form_control_1">العنوان  باللغه الانجليزيه</label>
    </div>

    <div class="form-group form-md-line-input form-md-floating-label{{ $errors->has('en_body') ? ' has-error' : 'has-success' }}">
        {!!Form::textarea('en_body',old('en_body'),['class'=>'form-control summernote'])!!}
        <label for="form_control_1">الموضوع  باللغه الانجليزيه</label>
    </div>
</div>

    <div class="form-group form-md-line-input form-md-floating-label{{ $errors->has('ar_body') ? ' has-error' : 'has-success' }}">
        {!!Form::textarea('ar_body',old('ar_body'),['class'=>'form-control summernote'])!!}
        <label for="form_control_1">النص  باللغه العربيه</label>
    </div>
</div>

<div class="form-actions noborder">
    <button type="submit" class="btn blue">حفظ</button>
    <button type="reset" class="btn default">الغاء</button>
</div>
