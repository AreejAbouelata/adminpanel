<div class="page-sidebar-wrapper" >
    <div class="page-sidebar navbar-collapse collapse" style=" margin-top: 2%;">

        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item start ">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-home"></i>
                    <span class="title">الرئيسيه</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">

                    <li class="nav-item start ">
                        <a href="{!!route('Settings.index')!!}" class="nav-link ">
                            <i class="icon-bar-chart"></i>
                            <span class="title">الاعدادت</span>
                        </a>
                    </li>
                    <li class="nav-item start ">
                        <a href="{!!route('Contact.index')!!}" class="nav-link ">
                            <i class="fa fa-box"></i>
                            <span class="title">الرسائل</span>
  <span class="badge badge-success">1</span>

                        </a>
                    </li>

                </ul>
            </li>
           
          {{--    <li class="heading">
                <h3 class="uppercase">صفحات الموقع</h3>
            </li>  --}}
{{--  users  --}}

            <li class="nav-item {{-- active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">الاعضاء</span>
                 {{--   <span class="selected"></span>--}}

                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{--active open--}}">
                        <a href="{!! route('Users.index')!!}" class="nav-link ">
                            <span class="title">كل الاعضاء</span>
                            <span class="badge badge-success">{!! \App\User::count() !!}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{!!route('Users.create')!!}" class="nav-link ">
                            <span class="title">اضافه عضو جديد</span>
                        </a>
                    </li>
                </ul>
            </li>
{{--  endusers  --}}

            {{--  Abouts  --}}

            <li class="nav-item {{-- active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">معلومات من نحن</span>
                    {{--   <span class="selected"></span>--}}

                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{--active open--}}">
                        <a href="{!! route('About.index')!!}" class="nav-link ">
                            <span class="title">كل معلومات من نحن</span>
                            <span class="badge badge-success">{!! \App\About::count() !!}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{!!route('About.create')!!}" class="nav-link ">
                            <span class="title">اضافه معلومه جديده</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--  Abouts  --}}


            {{--  Statistic  --}}

            <li class="nav-item {{-- active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">الاحصائيات</span>
                    {{--   <span class="selected"></span>--}}

                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{--active open--}}">
                        <a href="{!! route('Statistic.index')!!}" class="nav-link ">
                            <span class="title">كل الاحصائيات</span>
                            <span class="badge badge-success">{!! \App\Statistic::count() !!}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{!!route('Statistic.create')!!}" class="nav-link ">
                            <span class="title">اضافه احصائيه جديده</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--  Statistic  --}}


            {{--  Quotes  --}}

            <li class="nav-item {{-- active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">رائ العملاء</span>
                    {{--   <span class="selected"></span>--}}

                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{--active open--}}">
                        <a href="{!! route('Quote.index')!!}" class="nav-link ">
                            <span class="title">كل الاراء</span>
                            <span class="badge badge-success">{!! \App\Quote::count() !!}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{!!route('Quote.create')!!}" class="nav-link ">
                            <span class="title">اضافه رائ جديد</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--  Quote  --}}

            {{--  Sliders  --}}

            <li class="nav-item {{-- active open--}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">صور الصفحه الرئيسه</span>
                    {{--   <span class="selected"></span>--}}

                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  {{--active open--}}">
                        <a href="{!! route('Slider.index')!!}" class="nav-link ">
                            <span class="title">كل الصور</span>
                            <span class="badge badge-success">{!! \App\Slider::count() !!}</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{!!route('Slider.create')!!}" class="nav-link ">
                            <span class="title">اضافه صوره جديده</span>
                        </a>
                    </li>
                </ul>
            </li>
            {{--  Slider  --}}


        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>