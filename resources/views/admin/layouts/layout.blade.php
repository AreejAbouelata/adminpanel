<!DOCTYPE html >

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html dir="rtl">
<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="admin panel " name="description" />
    <meta content="" name="shaband" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
      <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{!!asset('public/admin/')!!}/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="{!!asset('public/admin/')!!}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />


    <link href="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{!!asset('public/admin/')!!}/assets/global/css/components-md-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/global/css/plugins-md-rtl.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{!!asset('public/admin/')!!}/assets/layouts/layout4/css/layout-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="{!!asset('public/admin/')!!}/assets/layouts/layout4/css/themes/default-rtl.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="{!!asset('public/admin/')!!}/assets/layouts/layout4/css/custom-rtl.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> 
    @yield('header')
</head>
<!-- END HEAD -->

<body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo page-md">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="index.html">
                <img src="{!!asset('public/admin/')!!}/assets/layouts/layout4/img/logo-light.png" alt="logo" class="logo-default" /> </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN PAGE TOP -->
        <div class="page-top">

            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
{{-- include notification to bar

@include('admin.layouts.notifications')

 --}}
                 <!-- END INBOX DROPDOWN -->

                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile"> {!!auth()->user()->name!!} </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                   {{--           <img alt="" class="img-circle" src="{!!asset('public/admin/')!!}/assets/layouts/layout4/img/avatar9.jpg" /  --}}> </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                {!!Form::open(['method'=>'post','route'=>'logout'])!!}

                            <button type=submit class="btn btn-primary"><i class="icon-key"></i> Log Out </button>
                                {!! Form::close()!!}
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->

                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->

    @include('admin.layouts.sidebar')
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>@yield('address')
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
                <!-- BEGIN PAGE TOOLBAR -->
            
                <!-- END PAGE TOOLBAR -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN PAGE BREADCRUMB -->
        {{--
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="#">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
             @yield('breadcrumb')
                --}}{{-- <li>
                    <a href="#">Blank Page</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span class="active">Page Layouts</span>
                </li> --}}{{--
            </ul>--}}
            <!-- END PAGE BREADCRUMB -->
            <!-- BEGIN PAGE BASE CONTENT -->
            <div class="portlet light portlet-fit bordered">
                @yield('content')
            </div>
            <!-- END PAGE BASE CONTENT -->
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 2017 &copy; ieasoft Theme By
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->

<!--[if lt IE 9]>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/respond.min.js"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/excanvas.min.js"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{!!asset('public/admin/')!!}/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{!!asset('public/admin/')!!}/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{!!asset('public/admin/')!!}/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{!!asset('public/admin/')!!}/assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{!!asset('public/admin/')!!}/assets/pages/scripts/table-datatables-responsive.js" type="text/javascript"></script>
 <script src="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap-summernote/summernote.js" type="text/javascript"></script>
 <script src="{!!asset('public/admin/')!!}/assets/global/plugins/bootstrap-summernote/lang/summernote-ar-AR.min.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{!!asset('public/admin/')!!}/assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
<script src="{!!asset('public/admin/')!!}/app.js"> </script>
<!-- END THEME LAYOUT SCRIPTS -->
<script>
    $(document).ready(function()
    {
        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    })
</script>
</body>
@yield('footer')

</html>