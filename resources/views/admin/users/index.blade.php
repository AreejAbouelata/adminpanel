
 @extends('admin.layouts.layout')
 @section('title')الاعضا@endsection
 @section('header')@endsection
 @section('content')
                    <div class="row">
                    <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-globe"></i>الاعضاء</div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="sample_3" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="all  text-center">الاسم</th>
                                                <th class="min-phone-l  text-center">الايميل</th>
                                                <th class="min-tablet text-center" >الاعدادت</th>
                                             
                                            </tr>
                                        </thead>
                                       <tbody>
                                           @foreach($users as $user)
                                            <tr>
                                                <td>{!!$user->name!!}</td>
                                                <td>{!!$user->email!!}</td>
                                                <td colspan="1">
                                                        <div class="margin-bottom-5">
                                                            <a href="{{route('Users.edit',[$user->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                                                <i class="fa fa-pencil"></i> edit</a>
                                                        </div>
                                                  
                                                    <div class="margin-bottom-5">
                                                    <form method="POST" action=
                                                    {!!route('Users.destroy',[$user->id])!!}>
                                                        {{ method_field('DELETE') }}
                                                         {{ csrf_field() }}
                                                        <button type="submit"  class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-danger"></i> delete</button>
                                                        
                                                    </form>
                                                </div>
                                                </td>
                                                
                                            </tr>
                                            @endforeach
                                        </body>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    
                    </div>
 
@endsection
 @section('footer')@endsection


