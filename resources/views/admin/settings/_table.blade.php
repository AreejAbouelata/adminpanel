<table class="table table-striped table-bordered table-hover dt-responsive" width="100%" {{--id="sample_3"--}} cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="all  text-center">الاسم</th>
        <th class="min-phone-l  text-center">القيمه باللغه العربيه</th>
        <th class="min-phone-l  text-center">القيمه باللغه الانجليزيه</th>

        <th class="min-tablet text-center" >خيارات</th>

    </tr>
    </thead>
    <tbody>
    @foreach($settings as $setting)
        <tr>
            <td class="text-center">{!!$setting->slug!!}</td>
            <td @if(in_array($setting->type,[1,2])) colspan="2" @endif  class="text-center">
                {!!$setting->ar_value!!}</td>
            @if(!in_array($setting->type,[1,2]))
            <td class="text-center">{!!$setting->en_value!!}</td>
            @endif
            <td colspan="1">
                <div class="margin-bottom-5">
                    <a href="{{route('Settings.edit',[$setting->id])}}" class="btn btn-sm green btn-outline filter-submit margin-bottom">
                        <i class="fa fa-pencil"></i> edit</a>
                </div>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>