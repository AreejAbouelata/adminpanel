<div class="form-body">
    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('value') ? ' has-error' : 'has-success' }} ">
        @if($setting->type==1)
     {!!Form::number('ar_value',old('ar_value'),['class'=>'form-control'])!!}

        @elseif($setting->type==2)
                {!!Form::email('ar_value',old('ar_value'),['class'=>'form-control'])!!}

         @else
        {!!Form::text('ar_value',old('ar_value'),['class'=>'form-control'])!!}
        @endif
                <label for="form_control_1">{!!$setting->slug!!} باللغه العربيه</label>

    </div>

    <div class="form-group form-md-line-input form-md-floating-label  {{ $errors->has('value') ? ' has-error' : 'has-success' }} " @if(in_array($setting->type,[2,1]))
        style="display: none" @endif>
        @if($setting->type==1)
            {!!Form::number('en_value',old('en_value'),['class'=>'form-control'])!!}
        @elseif($setting->type==2)
            {!!Form::email('en_value',old('en_value'),['class'=>'form-control'])!!}
        @else
            {!!Form::text('en_value',old('en_value'),['class'=>'form-control'])!!}
        @endif
        <label for="form_control_1">{!!$setting->slug!!} باللغه الانجليزيه</label>
    </div>
    <div class="form-actions noborder">
    <button type="submit" class="btn blue">حفظ</button>
    <button type="reset" class="btn default">الغاء</button>
</div>
</div>