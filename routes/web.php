<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index');
Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('Users', 'usersController');
    Route::resource('Settings','SettingController');
    Route::resource('About','AboutController');
    Route::resource('Statistic','StatisticController');
    Route::resource('Quote','QuoteController');
    Route::resource('Slider','SliderController');
    Route::resource('Contact','ContactController');
    Route::View('/','home')->name('dashboard');
});
