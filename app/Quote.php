<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable=[
        'ar_name',
        'en_name',
        'ar_job',
        'en_job',
        'rating',
        'ar_title',
        'en_title',
        'ar_body',
        'en_body',
        'created_by',
        ];

    public  static $rules=[
        'ar_name'=>'required|string|max:191|',
        'en_name'=>'required|string|max:191|',
        'ar_job'=>'nullable|string|max:191|',
        'en_job'=>'nullable|string|max:191|',
        'rating'=>'nullable|numeric|min:0|max:5|',
        'ar_title'=>'required|string|max:191|',
        'en_title'=>'required|string|max:191|',
        'ar_body'=>'required|string',
        'en_body'=>'required|string',
    ];
}
