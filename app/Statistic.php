<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable=['ar_name', 'en_name', 'percent',];

    public static  $rules=
        [
            'ar_name'=>'required|string|max:191',
            'en_name'=>'required|string|max:191',
            'percent'=>'required|numeric|min:1|max:100',
        ];


}
