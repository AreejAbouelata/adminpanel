<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class contact extends Mailable
{
    use Queueable, SerializesModels;
    public $msg;
    public $fro;
    public function __construct($msg,$fro)
    {
        $this->msg=$msg;
        $this->fro=$fro;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('emails.contact')->from($this->fro)->subject('KIBD')->with('msg',$this->msg);
    }
}
