<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class activate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $sub;
    public $msg;
    public $attach;
    public function __construct($message,$subject='active account',$attach=null)
    {
        $this->sub=$subject;
         $this->msg=$message;
        $this->attach=$attach;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->markdown('emails.email',['msg'=>$this->msg])->subject($this->sub);
    }
}
