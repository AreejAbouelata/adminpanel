<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable=[
        'en_text', 'ar_text', 'en_body', 'ar_body', 'img', 'user_id','file'
    ];
    public static $rules=[
        'en_text'=>'required|string|max:191',
        'ar_text'=>'required|string|max:191',
        'en_body'=>'required|string|',
        'ar_body'=>'required|string|',
        'img'=>'required',
    ];
    public static $updateRules=[
        'en_text'=>'required|string|max:191',
        'ar_text'=>'required|string|max:191',
        'en_body'=>'required|string|',
        'ar_body'=>'required|string|',
        'img'=>'nullable',
    ];
    public function created_by(){
        return $this->belongsTo('App\User','user_id');
    }
}
