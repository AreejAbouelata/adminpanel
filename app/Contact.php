<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable=[
        'name', 'email', 'subject', 'read_at', 'reply', 'user_id', 'message',
    ];

    public static $rules=[
        'name'=>'required|string|max:191',
        'email'=>'required|email|max:191',
        'subject'=>'required|string|max:191',
        'reply'=>'nullable|string',
        'message'=>'required|string',
    ];
    public static $updateRules=[
        'reply'=>'required|string',
    ];

    public function created_by(){
        return $this->belongsTo('App\User','user_id');
    }
}
