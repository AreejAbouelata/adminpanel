<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = ['ar_body', 'user_id', 'en_body','en_name', 'ar_name'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static $rules = [
        'ar_name' => 'required|string|max:191',
        'ar_body' => 'required|string',
        'en_name' => 'required|string|max:191',
        'en_body' => 'required|string'
    ];
}
