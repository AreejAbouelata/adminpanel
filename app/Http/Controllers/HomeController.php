<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use App\Setting;
use App\subscriber;
use App\Team;
use Mail;
use App\Mail\contact;



class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings=Setting::all()->pluck('value','name');
        $days=Day::with('lectures')->get();
        $Teams=Team::all();

        return view('welcome',compact('settings','days','Teams'));
    }
    public function subscribe(Request $request){

           $requests=$request->validate([
            'email'=>'required|string|email|max:191',
             'name'=>'required|string|max:191',
              'job'=>'required|string|max:191',
             'phone'=>'required|numeric'
        ]);
$msg='قام '.$request->name.' بالاشتراك ';
       $s= Subscriber::forceCreate($request->except('_token'));
       $site=Setting::find(4)->value;
            Mail::to($site)->send(new contact($msg,$requests['email']));

               return  redirect()->route('Subscriber.index')->with('success','تم الاضافه بنجاح');

    }
}
