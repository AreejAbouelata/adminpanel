<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::all();
        return view('admin.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('admin.users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $requests= $request->validate(User::$rules);
       $requests['password']=Hash::make($request->password);
        $newUser=User::create($requests);
        return redirect()->route('Users.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                    $user= User::find($id);
                    if(is_null($user)){
                        return back()->with('error','هذا العضو غير موجود');
                    }
                    return view('admin.users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $user= User::find($id);
          $rules=[
                   'name' => 'required|string|max:255',
                  'email' => 'required|string|email|max:255|unique:users,email,'.$id,
               ];
                    if(is_null($user)){
                        return back()->with('error','هذا العضو غير موجود');
                    }

                     if($request->has('password')){
                   $rules['password']=User::$rules['password'];
               }
                  $requests =  $request->validate($rules);
               if(isset($requests['password'])){
                   $requests['password']=Hash::make($requests['password']);
               }
           $update= $user->fill($requests)->save();
               return redirect()->route('Users.index')->with('success','تم التعديل بنجاح');
            }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $user=  User::find($id);
      if(is_null($user)){
          return back()->with('error','هذا العضو غير موجود');
      }
      $user->delete();
      return back()->with('success','تم حذف العضو');
    }
}
