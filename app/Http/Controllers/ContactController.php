<?php

namespace App\Http\Controllers;

use App\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Contacts = Contact::all();
        return View('admin.Contact.index', compact('Contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
  /*  public function create()
    {
        return view('admin.Contact.create');
    }*/

 /*   public function store(Request $request)
    {
        $requests=$request->validate(Contact::$rules);
        $requests['user_id']=Auth::id();
        Contact::create($requests);
        return redirect()->route('Contact.index');
    }*/

    public function show(Contact $Contact)
    {


    }

    public function edit($id)
    {
        $Contact= Contact::find($id);
        $Contact->fill(['read_at'=>Carbon::today()])->save();
        return view('admin.Contact.edit',compact('Contact'));
    }
    public function update(Request $request, $id)
    {
        $Contact=Contact::find($id);
        $requests=$request->validate(Contact::$updateRules);
        $Contact->fill($requests)->save();
        return redirect()->route('Contact.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $Contact
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Contact=  Contact::find($id);
        if(is_null($Contact)){
            return back()->with('error','هذا الموضوع غير موجود');
        }
        $Contact->delete();
        return back()->with('success','تم الحذف ');
    }
}
