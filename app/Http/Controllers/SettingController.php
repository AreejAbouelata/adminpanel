<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::all();
        return View('admin.settings.index', compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $setting = Setting::find($id);
        if (is_Null($setting)) {
            return back()->with('error', 'هذا الاعداد غير موجود');
        }
        return View('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);

        $rules = [
            'ar_value' => 'required|string',
            'en_value' => 'required|string',];
        if ($setting->type == 1) {
            $rules = [
                'ar_value' => 'required|numeric',
                'en_value' => 'required|numeric',
            ];
        } elseif ($setting->type == 2) {
            $rules = [
                'ar_value' => 'required|email|max:191',
                'en_value' => 'required|email|max:191',
                ];
        }
        $requests = $request->validate($rules);
        if (in_array($setting->type, [1, 2])) {
            $requests['en_value'] = $request->ar_value;
        }
        $set = $setting->update($requests);
        return redirect()->route('Settings.index')->with('success', 'تم التعديل');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
