<?php

namespace App\Http\Controllers;

use App\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Quotes = Quote::all();
        return View('admin.Quote.index', compact('Quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Quote.create');
    }

    public function store(Request $request)
    {
        $requests=$request->validate(Quote::$rules);
        $requests['user_id']=Auth::id();
        Quote::create($requests);
        return redirect()->route('Quote.index');
    }

    public function show(Quote $Quote)
    {


    }

    public function edit($id)
    {
        $Quote= Quote::find($id);

        return view('admin.Quote.edit',compact('Quote'));

    }
    public function update(Request $request, $id)
    {
        $Quote=Quote::find($id);
        $requests=$request->validate(Quote::$rules);
        $Quote->fill($requests)->save();
        return redirect()->route('Quote.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Quote  $Quote
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Quote=  Quote::find($id);
        if(is_null($Quote)){
            return back()->with('error','هذا الموضوع غير موجود');
        }
        $Quote->delete();
        return back()->with('success','تم الحذف ');
    }
}
