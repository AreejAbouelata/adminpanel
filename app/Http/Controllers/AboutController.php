<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = About::all();
        return View('admin.about.index', compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about.create');
    }

    public function store(Request $request)
    {
        $requests=$request->validate(About::$rules);
        $requests['user_id']=Auth::id();
        About::create($requests);
        return redirect()->route('About.index');
    }

    public function show(About $about)
    {


    }

    public function edit($id)
    {
       $about= About::find($id);

       return view('admin.about.edit',compact('about'));

    }
    public function update(Request $request, $id)
    {
        $about=About::find($id);
        $requests=$request->validate(About::$rules);
        $about->fill($requests)->save();
        return redirect()->route('About.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about=  About::find($id);
        if(is_null($about)){
            return back()->with('error','هذا الموضوع غير موجود');
        }
        $about->delete();
        return back()->with('success','تم الحذف ');
    }
}
