<?php

namespace App\Http\Controllers;

use App\Statistic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Statistics = Statistic::all();
        return View('admin.Statistic.index', compact('Statistics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Statistic.create');
    }

    public function store(Request $request)
    {
        $requests=$request->validate(Statistic::$rules);

        Statistic::create($requests);
        return redirect()->route('Statistic.index');
    }

    public function show(Statistic $Statistic)
    {


    }

    public function edit($id)
    {
        $Statistic= Statistic::find($id);

        return view('admin.Statistic.edit',compact('Statistic'));

    }
    public function update(Request $request, $id)
    {
        $Statistic=Statistic::find($id);
        $requests=$request->validate(Statistic::$rules);
        $Statistic->fill($requests)->save();
        return redirect()->route('Statistic.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Statistic  $Statistic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Statistic=  Statistic::find($id);
        if(is_null($Statistic)){
            return back()->with('error','هذا الموضوع غير موجود');
        }
        $Statistic->delete();
        return back()->with('success','تم الحذف ');
    }
}
