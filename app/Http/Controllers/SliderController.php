<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Sliders = Slider::all();
        return View('admin.Slider.index', compact('Sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Slider.create');
    }

    public function store(Request $request)
    {
        $requests=$request->validate(Slider::$rules);
        if($request->has('img')||$request->hasFile('img')){
            $file=$request->img;

           $fileName= $file->getClientOriginalName();
            $file->move(public_path('images/sliders'),$fileName);
            $requests['img']='/public/images/sliders/'.$fileName;
        }
        $requests['user_id']=Auth::id();
        Slider::create($requests);
        return redirect()->route('Slider.index');
    }

    public function show(Slider $Slider)
    {


    }

    public function edit($id)
    {
        $Slider= Slider::find($id);
        return view('admin.Slider.edit',compact('Slider'));
    }
    public function update(Request $request, $id)
    {
        $Slider=Slider::find($id);
        $requests=$request->validate(Slider::$updateRules);
        if($request->has('img')||$request->hasFile('img')){
            $file=$request->img;
            $fileName= $file->getClientOriginalName();
            $file->move(public_path('images/sliders'),$fileName);
            $requests['img']='/public/images/sliders/'.$fileName;
            $oldImage=$Slider->img;
            //File::delete($oldImage);
        }
        $Slider->fill($requests)->save();
        return redirect()->route('Slider.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $Slider
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Slider=  Slider::find($id);
        if(is_null($Slider)){
            return back()->with('error','هذا الموضوع غير موجود');
        }
        //File::delete('public/images/sliders/'.$Slider->img);
        $Slider->delete();
        return back()->with('success','تم الحذف ');
    }
}
